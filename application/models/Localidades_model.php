<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Localidades_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE localidades');
		return $this->db->insert("localidades",$data);
	}

	public function update($data,$id){
		$this->db->where("id_localidad",$id);
		return $this->db->update("localidades",$data);
	}

	public function getLocalidad($id){
		$this->db->select("l.*");
		$this->db->from("localidades l");
		$this->db->where("l.id_localidad",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getLocalidades(){
		$this->db->select("l.*");
		$this->db->from("localidades l");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_localidad", $id);
		$this->db->db_debug = false;
		if($this->db->delete("localidades")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un cliente activo!");
		}
	}

}
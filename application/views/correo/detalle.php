<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0"><?php echo $asunto; ?></h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>chat">correo</a></li>
                <li class="breadcrumb-item active"><?php echo $asunto; ?></li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>chat" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">

    <div class="row">
        
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">lectura del mensaje</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                
                <form action="<?php echo base_url();?>nuevo-correo" method="POST">
                    <div class="form-group">
                        <label class="form-control-label">de usuario</label>
                        <input readonly="readonly" type="text" name="de" class="form-control <?php echo form_error('de') ? 'is-invalid':''?>"  value="<?php echo form_error('de') ? set_value('de'): $de; ?>">
                        <div class="invalid-feedback"><?php echo form_error('de'); ?></div>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">asunto</label>
                        <input readonly="readonly" type="text" name="asunto" class="form-control <?php echo form_error('asunto') ? 'is-invalid':''?>"   value="<?php echo form_error('asunto') ? set_value('asunto'): $asunto; ?>">
                        <div class="invalid-feedback"><?php echo form_error('asunto'); ?></div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">mensaje</label>
                        <textarea readonly="readonly" rows="4" name="mensaje" class="form-control <?php echo form_error('mensaje') ? 'is-invalid':''?>" placeholder="mensaje"><?php echo  form_error('mensaje') ? set_value('mensaje') : $mensaje; ?></textarea>
                        <div class="invalid-feedback"><?php echo form_error('mensaje'); ?></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Fecha de envio </label>
                                <input readonly="readonly" type="date" name="fecha_envio" class="form-control <?php echo form_error('fecha_envio') ? 'is-invalid':''?>"  value="<?php echo form_error('fecha_envio') ? set_value('fecha_envio'): $fecha_envio; ?>">
                                <div class="invalid-feedback"><?php echo form_error('fecha_envio'); ?></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Hora de envio</label>
                                <input readonly="readonly" type="text" name="hora_envio" class="form-control <?php echo form_error('hora_envio') ? 'is-invalid':''?>"   value="<?php echo form_error('hora_envio') ? set_value('hora_envio'): $hora_envio; ?>">
                                <div class="invalid-feedback"><?php echo form_error('hora_envio'); ?></div>
                            </div>
                        </div>
                    </div>

                    

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success mt-4">responder</button>
                    </div>

                </form>
                </div>
            </div>
        </div>

       

    </div>


<!-- Main content -->
<?php              
 //busco la cantidad de mensajes no leidos
                $this->db->select("*");
                $this->db->from("mensaje");
                $this->db->where("para=",$this->session->userdata("usuario"));
                $this->db->where("leido =","no");
              $numero = $this->db->get()->num_rows();
                   
    ?>

<div class="main-content" id="panel">
<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-light bg-primary border-bottom">
    <div class="container-fluid">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Search form -->
        <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
        <div class="form-group mb-0">
            <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Search" type="text">
            </div>
        </div>
        <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </form>
        <!-- Navbar links -->
        <ul class="navbar-nav align-items-center  ml-md-auto ">
        <li class="nav-item d-xl-none">
            <!-- Sidenav toggler -->
            <div class="pr-3 si
            denav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
            </div>
            </div>
        </li>
        <li class="nav-item d-sm-none">
            <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
            <i class="ni ni-zoom-split-in"></i>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"><?php echo $numero ?></i>
            </a>
            <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
            <!-- Dropdown header -->
            <div class="px-3 py-3"  >
           
           
                <h6 class="text-sm text-muted m-0">Tienes <strong class="text-primary"><?php echo $numero ?></strong> Notificaciones.</h6>
                
                <?php foreach($data2 as $value):?>
                <?php if($value->para==$this->session->userdata("usuario")){?>
                    <div class="px-3 py-3" style="border: 6px solid black" >
                <a href="<?php echo base_url()."correo/leer/".$value->id; ?>"><h6>EL USUARIO:<?php echo $value->de?>  </h6>
                
                <h6>MENSAJE:    <?php echo $value->mensaje?></h6>
                        </a>
                </div>
                 <?php }?>
                <?php endforeach; ?>
                
                
            </div>
            <!-- View all -->
            <a href="<?php echo base_url()."chat" ?>" class="dropdown-item text-center text-primary font-weight-bold py-3">Ver todos</a>
            </div>
        </li>
        </ul>
        <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
        <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                
                    <img src="<?php echo base_url(); ?>assets/img/user/img1.png">
                 
                </span>
                <div class="media-body  ml-2  d-none d-lg-block">
                <span class="mb-0 text-sm  font-weight-bold"><?php echo $this->session->userdata("nombre"); ?></span>
                </div>
            </div>
            </a>
            <div class="dropdown-menu  dropdown-menu-right ">
            <div class="dropdown-header noti-title">
                <h6 class="text-overflow m-0">Bienvenido!</h6>
            </div>
            <a href="<?php echo base_url(); ?>perfil" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>Mi perfil</span>
            </a>
            <a href="" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Opciones</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url(); ?>cerrarsesion" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Cerrar Sesión</span>
            </a>
            </div>
        </li>
        </ul>
    </div>
    </div>
</nav>
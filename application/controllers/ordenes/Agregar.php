<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model("Ordenes_model");
		$this->load->model("Usuario_model");
		$this->load->model("Log_model");
		if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{   	$data2 = array("data2" => $this->Usuario_model->getMensajes()); 
		
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('ordenes/agregar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/ordenes'); 
    }
    
    public function save(){

		$origen = $this->input->post("origenId");
		$destino = $this->input->post("destino");
		$estado = $this->input->post("estado");
		$cliente = $this->input->post("cliente");
		$vehiculo = $this->input->post("vehiculo"); 
		$conductor = $this->input->post("conductor");
		$usuarioactual=$this->session->userdata("nombre");
		
					$data  = array(
				'origen' => $origen,
                'destino' => $destino,
				'estado' => $estado, 
				'cliente' => $cliente,
                'vehiculo' => $vehiculo,
                'conductor' => $conductor,               
			);
			$data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Agregar Ordenes',
				'descripcion'=>'el usuario '.$usuarioactual.' Agrego una orden para vehiculo: '.$vehiculo.'',

			);
			$this->Log_model->save($data2);

			$this->Ordenes_model->save($data);

			$this->session->set_flashdata("success","Se guardó correctamente!");
			redirect(base_url()."ordenes");
		
		
	
	}

	
}

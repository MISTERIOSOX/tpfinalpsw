<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Vehiculo_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
    }

    public function index()
    {       
        $data = array("data" => $this->Vehiculo_model->getVehiculos()); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav');
        $this->load->view('vehiculo/lista',$data,$data2);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/vehiculo');
    }
    

    public function delete($id){
            
        $resp=$this->Vehiculo_model->delete($id);
        $this->session->set_flashdata($resp[0],$resp[1]);
        redirect(base_url()."vehiculos");
                    
    }

    public function getData(){
        $resp = $this->Vehiculo_model->getRols();
        if($resp){
            echo json_encode($resp);
        }
    }
 
}
